package com.vito.careworker.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vito.careworker.R;
import com.vito.careworker.data.FunItem;

/**
 * Created by pc on 2016/7/19.
 */
public class QuickEntriesAdapter extends BaseViewAdapter<FunItem> {
    Context mContext;
    boolean mLoadLocalImg;

    public QuickEntriesAdapter(Context context, int layoutId) {
        super(context, layoutId);
        mContext = context;
    }

    public void setmLoadLocalImg(boolean mLoadLocalImg) {
        this.mLoadLocalImg = mLoadLocalImg;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(mLayoutId, null);
            holder.imageView1 = (ImageView) convertView.findViewById(R.id.image);
            holder.textView1 = (TextView) convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textView1.setText(mData.get(position).getModulename());

        if (mData.get(position).getModulepic() != null) {

            String imgurl = mData.get(position).getModulepic();
            if (imgurl.startsWith("/"))
                imgurl = mData.get(position).getModulepic().substring(1);
            else if (imgurl.startsWith(""))
                imgurl = mData.get(position).getModulepic();

//            Glide.with(mContext)
//                    .load(imgurl)
//                    .placeholder(R.drawable.sq_loading)
//                    .error(R.drawable.sq_loading)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .fitCenter()
//                    .crossFade()
//                    .into(holder.imageView1);

            Glide.with(holder.imageView1.getContext()).load(imgurl)
                    .centerCrop()
                    .crossFade()
                    .into(holder.imageView1);


        }
        return convertView;
    }

    public static class ViewHolder {
        public ImageView imageView1;
        public TextView textView1;
    }
}

package com.vito.careworker.utils;

import java.util.HashMap;

/**
 * Created by pc on 2016/11/22.
 */

public class Comments {

    /* test base url 外网地址*/
    public static final String BASE_URL = "http://123.138.87.158:60081/";
//    public static final String BASE_URL = "http://192.168.1.55:8080/oa.web/";
    /* test base url 内网地址*/
//    public static final String BASE_URL = "http://192.168.1.248:60081/";
    /* base url 正式地址*/
//    public static final String BASE_URL = "http://email.bkvito.com:64000/";
    /* test base url 临时测试地址*/
//    public static final String BASE_URL = "http://192.168.1.43:8081/oa.web/";
//    public static final String BASE_URL = "http://192.168.1.54:8080/oa.web/";
//    public static final String BASE_URL = "http://192.168.1.61/";
    /*
    * local configs
    */
    public static final String FG_HOME_DATA = "/json/vito_home_tab_data.json";
    public static final String HOME_MORE_FUN_CONFIG = "/json/vito_all_actions.json";
    public static final String MY_INFO_PATH = "/json/vito_my_info.json";


    public static final String GET_UPDATE_URL = BASE_URL + "base/apk/apkManage/checkUpApk.htm";
    public static final String SYS_TYPE = "swybg";
    public static final int START_WAIT_TIME = 3000;

    /*SMS*/
    public static final String SMS_URL = BASE_URL + "/oa/base/sendVerificationCode.htm";


    /* Login */
    public static final String LOGIN_URL = BASE_URL + "base/authoriza/login/userLoginPhone.htm";
    public static final String GET_TIME_STAMP_URL = BASE_URL + "base/authoriza/login/getTimestamp.htm";
    public static final String YTX_LOGIN_SUCCESS = BASE_URL + "base/msg/ytx/ytxLoginSuc.htm";
    /*register*/
    public static final String CHECK_TELNEMBUER_URL = BASE_URL + "oa/base/authoriza/user/queryUserCount.htm";
    public static final String REGISTER_SECOND_URL = BASE_URL + "oa/base/saveRegisterUserByDeptCodeSecond.htm";
    public static final String REGISTER_FIRST_URL = BASE_URL + "oa/base/saveRegisterUserByDeptCode.htm";
    public static final String REGISTER_DEPT_URL = BASE_URL + "oa/base/saveRegisterUser.htm";

    /* CloudDisk */
    public static final String DIR_FROM_CLOUD_URL = BASE_URL + "fs/file/list.htm";
    public static final String DOWNLOAD_FILE_URL = BASE_URL + "fs/file/download.htm";
    public static final String SPACE_SIZE_URL = BASE_URL + "fs/space/size.htm";
    public static final String PERSONAL_FOLDER_CREATE_URL = BASE_URL + "fs/user/folder/create.htm";
    public static final String GROUP_FOLDER_CREATE_URL = BASE_URL + "fs/org/folder/create.htm";
    public static final String UPDATE_RENAME_URL = BASE_URL + "fs/file/update.htm";
    public static final String FILE_DELETE_URL = BASE_URL + "fs/file/delete.htm";
    public static final String QUERY_GROUP_BY_USER_URL = BASE_URL + "oa/fsgroup/queryFSGroupByUser.htm";
    public static final String UPLOAD_FILE_URL = BASE_URL + "fs/user/file/upload.htm";
    public static final String GROUP_UPLOAD_FILE_URL = BASE_URL + "fs/org/file/upload.htm";
    public static final String GET_DEFAULT_SPACE_URL = BASE_URL + "fs/default/space/get.htm";
    public static final String CHANGE_DEFAULT_SPACE_SIZE_URL = BASE_URL + "fs/default/space/update.htm";
    public static final String SEARCH_FILE_URL = BASE_URL + "fs/file/list.htm";
    public static final String USED_SPACE_SIZE = BASE_URL + "fs/usage/list.htm";
    public static final String CHANGE_INFO_URL = BASE_URL + "fs/file/change/list.htm";

    /* AD */
    public static final String BannerImage_DATA_PATH = "/json/vitoAd.json";
    public static final String AD_CLICK_NUM_PATH = BASE_URL + "ecs/common/ad/updAd.htm";
    public static final String AD_IMG_PATH = BASE_URL + "ecs/common/ad/getAd.htm";

    /* calendar and conference */
    public static final String CALENDAR_EVENT_URL =BASE_URL + "off/event/list.htm";
    public static final String EVENT_LIST_URL = BASE_URL + "off/event/base/list.htm";
    public static final String FOCUS_EVENT_URL = BASE_URL + "off/event/focus/list.htm";
    public static final String ADD_EVENT_URL = BASE_URL + "off/event/add.htm";
    public static final String DELETE_EVENT_URL = BASE_URL + "off/event/delete.htm";
    public static final String ADD_CALENDAR_URL = BASE_URL + "off/event/base/add.htm";
    public static final String DELETE_CALENDAR_URL = BASE_URL + "off/event/base/delete.htm";
    public static final String FOCUS_EVENT_UPDATE_URL = BASE_URL + "off/event/focus/update.htm";
    public static final String USER_LIST_URL = BASE_URL + "off/event/base/user/list.htm";
    public static final String EVENT_USER_LIST_URL = BASE_URL + "off/event/user/list.htm";
    public static final String BASE_EVENT_UPDATE_URL = BASE_URL + "off/event/base/update.htm";
    public static final String EVENT_UPDATE_URL = BASE_URL + "off/event/update.htm";
    public static final String GET_EVENT_INFO_URL = BASE_URL + "off/event/get.htm";

    /* sign_on */
    public static final String SIGN_ON_REQUEST_URL = BASE_URL + "oa/att/checkIn.htm";
    public static final String SIGN_ON_HISTORY_URL = BASE_URL + "oa/att/user/attendance/log.htm";
    public static final String SIGN_ON_USER_INFO_URL = BASE_URL + "oa/att/user/attendance.htm";

    /* attendance */
    public static final String ATTENDANCE_PERMISSION_URL = BASE_URL + "base/authoriza/func/filter.htm";

    /* fun list */
//    public static final String NAVIGATION_URL = BASE_URL + "ecs/common/module/getModule.htm";
    public static final String NAVIGATION_URL = /*BASE_URL +*/ "/json/vito_moudles_test.json";

    /* public */
    public static final String PUBLIC_URL = BASE_URL + "oa/email/invoke.htm";

    /* email */
    public static final String CHECK_EMAIL_EXIST_URL = BASE_URL + "oa/email/checkUserMailExist.htm";
    public static final String CHECK_EMAIL_REPEAT_URL = BASE_URL + "oa/email/checkUserMailRepeat.htm";
    public static final String CHECK_EMAIL_SAVE_URL = BASE_URL + "oa/email/saveUserMail.htm";

    public static final String GET_EMAIL_INBOX_URL = BASE_URL + "oa/email/getInboxEmail.htm";
    public static final String GET_EMAIL_SENDBOX_URL = BASE_URL + "oa/email/getSentEmail.htm";
    public static final String GET_EMAIL_DRAFTBOX_URL = BASE_URL + "oa/email/getDraftEmail.htm";
    public static final String GET_EMAIL_DETAIL_URL = BASE_URL + "oa/email/getViewEmail.htm";
    public static final String DELETE_EMAIL_URL = BASE_URL + "oa/email/deleteEmail.htm";
    public static final String DELETE_EMAIL_BATCH_URL = BASE_URL + "oa/email/delBatchEmail.htm";
    public static final String GET_EMAILSIGN_URL = BASE_URL + "oa/email/getEmailSign.htm";
    public static final String SAVE_EMAIDRAFT_METHOD_NAME = "saveDraftEmail";
    public static final String REPLY_EMAIl_METHOD_NAME = "replyEmail";
    public static final String REPEAT_EMAIl_METHOD_NAME = "forwardEmail";
    public static final String SEND_DRAFT_METHOD_NAME = "draftSendEmail";
    public static final String RESAVE_DRAFT_METHOD_NAME = "saveDraftAgainEmail";
    public static final String SAVE_REPLY_FORWARD_DRAFT_METHOD_NAME = "saveReplyForwardDraftEmail";
    public static final String SEND_EMAIL_METHOD_NAME = "sendEmail";

    public static final String UPLOAD_EMAI_URL = BASE_URL + "oa/email/uploadAttachment.htm";

    public static final String EMAIL_INNER_TREE = BASE_URL + "oa/emailContacts/getInnerContactsTree.htm";
    public static final String EMAIL_EXTERNAL_TREE = BASE_URL + "oa/emailContacts/getEmailContactsTree.htm";
    public static final String EMAIL_DOWNLOAD_URL = BASE_URL + "oa/email/downloadFile.htm";

    /*personal info*/
    public static final String PERSONAL_INFO_CHANGE = BASE_URL + "base/authoriza/user/update.htm";
    public static final String PERSONAL_PWD_CHANGE = BASE_URL + "base/authoriza/user/updPsd.htm";
    public static final String UPLOAD_IMG_URL = BASE_URL + "base/authoriza/fileup/uploadFile.htm";

    /* upload */
    public static final String UPLOAD_IMG_PATH = BASE_URL + "base/authoriza/fileup/upload.htm";

    /* notice*/
    public static final String QUERY_NOTICE_PATH=BASE_URL+"oa/notice/queryReceiveNotice.htm";
    public static final String READ_NOTICE_PATH=BASE_URL+"oa/notice/readNotice.htm";
    public static final String NOTICECOUNT_PATH=BASE_URL+"oa/notice/queryReceiveNoticeCount.htm";
    /**
     * 登录的userId
     */
    public static final String USERID = "loginid";
    /**
     * 用户注册手机号
     */
    public static final String TELNUMBER = "telnumber";
    /**
     * 登录密码
     */
    public static final String PASSWORD = "password";

    /**
     * 是否自动登录
     */
    public static final String AUTOLOGIN = "isAutoLogin";

    /**
     * 是否记住密码
     */
    public static final String REMEMBERPASSWORD = "isRememberPassword";
    /**
     * 友盟推送相关
     */
    public static final String UPDATE_DEVICE_TOKEN = BASE_URL + "base/msg/mq/updUserDeviceInfo.htm";
    public static final String UMENG_APP_NAME = "swybg";
    /**
     * 联系人相关
     */
    public static final String CONTACT_BASE_DATA = "/json/vito_contact_base_data.json";
    public static final String CONTACT_DATA = BASE_URL + "oa/oauser/queryUser.htm";
    public static final String CONTACT_AND_DEPT_DATA = BASE_URL + "oa/oadept/queryDeptUser.htm";
    public static final String CONTACT_DETAIL = BASE_URL + "oa/oauser/queryUser.htm";
    public static final String CONTACT_JOB_INFO = BASE_URL + "oa/oarole/queryOARole.htm";
    public static final String CONTACT_DEPARTMENT = BASE_URL + "base/authoriza/dept/getTreeall.htm";
    public static final String GROUP_USER = BASE_URL + "oa/fsgroup/getGroupDetail.htm";
    public static final String DISBAND_GROUP = BASE_URL + "oa/fsgroup/deleteFSGroup.htm";
    public static final String QUIT_GROUP = BASE_URL + "oa/fsgroup/deleteGroupUser.htm";
    public static final String UPDATE_GROUP_INFO = BASE_URL + "oa/fsgroup/updateFSGroup.htm";
    /**
     * 群组
     */
    public static final String GROUP_ALL = BASE_URL + "oa/fsgroup/getAllGroupList.htm";
    public static final String SAVE_GROUP = BASE_URL + "oa/fsgroup/saveFSGroup.htm";
    public static final String GROUP_RELATION = BASE_URL + "oa/fsgroup/queryUserGroupIds.htm";
    /**
     * 分页数据 单页容量
     */
    public static final int PAGE_SIZE = 10;
    public static final HashMap<String, String> sParticularContacts = new HashMap<String, String>() {{
        put(ParticularMessages.SENDER_APPROVAL, "审批");
        put(ParticularMessages.SENDER_NOTICE, "公告");
        put(ParticularMessages.SENDER_MEETING, "会议");
        put(ParticularMessages.SENDER_EMAIL, "邮件");
        put(ParticularMessages.SENDER_CALENDAR, "日历");
        put(ParticularMessages.SENDER_ATTENDANCE, "考勤");
        put(ParticularMessages.SENDER_DOCUMENT, "公文");
        put(ParticularMessages.SENDER_CLOUD, "云盘");
    }};
    /*
    * badgeview type
    */
    public static String mBadgeTypes[] = {"system", "community"};
    public static String[] EMAIL_REPLY_OPER_NAMES = {
            "回复", "全部回复", "转发"
    };
    public static String EMAIL_REPLY_OPERS_NEW = "new";
    public static String EMAIL_REPLY_OPERS_REPLY = "reply";
    public static String EMAIL_REPLY_OPERS_REPLYALL = "replyall";
    public static String EMAIL_REPLY_OPERS_REPEAT = "repeat";
    public static String[] EMAIL_REPLY_OPERS = {
            EMAIL_REPLY_OPERS_REPLY, EMAIL_REPLY_OPERS_REPLYALL, EMAIL_REPLY_OPERS_REPEAT
    };
    public static String[] EMAIL_DEL_OPER_NAMES = {
            "删除", "彻底"
    };
    public static String EMAIL_DEL_OPERS_DEL = "delete";
    public static String EMAIL_DEL_OPERS_DELALL = "deleteall";
    public static String[] EMAIL_DEL_OPERS = {
            EMAIL_DEL_OPERS_DEL, EMAIL_DEL_OPERS_DELALL
    };
    public static String EMAIL_DOWNLOAD_DIR = "vito_email";
    public static String IMAGE_START = "/download";

    public class ParticularMessages {
        //审批
        public static final String SENDER_APPROVAL = "10000";
        //公告
        public static final String SENDER_NOTICE = "10001";
        //会议
        public static final String SENDER_MEETING = "10002";
        //邮件
        public static final String SENDER_EMAIL = "10003";
        //日历
        public static final String SENDER_CALENDAR = "10004";
        //考勤
        public static final String SENDER_ATTENDANCE = "10005";
        //公文
        public static final String SENDER_DOCUMENT = "10006";
        //云盘
        public static final String SENDER_CLOUD = "10007";
    }

}

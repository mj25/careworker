package com.vito.careworker.controller;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.vito.careworker.R;
import com.vito.careworker.interfaces.PullRefreshParentCallBack;

/**
 * Created by lenovo24 on 2017/4/21.
 */

public class ViewPagerController extends BaseRefreshableCtrller {

    private ViewPager mPager;
    private final Context mContext;

    public ViewPagerController(Context activity, ViewGroup rootView, PullRefreshParentCallBack parentrefresh) {
        super(activity, rootView, parentrefresh);
        LinearLayout ll = (LinearLayout) View.inflate(activity, R.layout.view_pager_home, rootView);
        mContext = activity;
        mPager = (ViewPager) ll.findViewById(R.id.view_pager);
        mPager.setAdapter(new MyAdapter());
    }

    @Override
    public void onPullDownToRefresh() {

    }

    @Override
    public void onPullUpToRefresh() {

    }

    private class MyAdapter extends PagerAdapter{

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_view_pager_nurse, null);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}

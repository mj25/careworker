package com.vito.careworker.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.flyco.banner.anim.select.ZoomInEnter;
import com.flyco.banner.transform.DepthTransformer;
import com.flyco.banner.widget.Banner.base.BaseBanner;
import com.vito.base.entity.BannerItem;
import com.vito.base.jsonbean.VitoJsonTemplateBean;
import com.vito.base.ui.fragments.FragmentFactory;
import com.vito.base.ui.widget.banner.SimpleImageBanner;
import com.vito.base.utils.FileUtils;
import com.vito.base.utils.ToastShow;
import com.vito.base.utils.network.httplibslc.JsonLoaderCallBack;
import com.vito.base.utils.network.httplibslc.RequestVo;
import com.vito.base.utils.network.jsonpaser.JsonLoader;
import com.vito.careworker.R;
import com.vito.careworker.data.VitoAdBean;
import com.vito.careworker.interfaces.PullRefreshParentCallBack;
import com.vito.careworker.utils.Comments;

import org.codehaus.jackson.type.TypeReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lenovo on 2016/6/29.
 */
public class ImageBannerCtrller extends BaseRefreshableCtrller implements JsonLoaderCallBack {

    SimpleImageBanner mSimpleImageBanner;
    VitoAdBean.VitoAdData mAdData;
    JsonLoader mJsonLoader;
    int GET_FROM_LOCAL = 0;
    int GET_FROM_SERVER = 1;
    int ADD_CLICK = 2;

    public ImageBannerCtrller(Activity activity, ViewGroup rootView, PullRefreshParentCallBack in_callback) {
        super(activity, rootView, in_callback);
        LinearLayout ll = (LinearLayout) View.inflate(activity, R.layout.banner_imge, rootView);
        mSimpleImageBanner = (SimpleImageBanner) ll.findViewById(R.id.sib_simple_usage);

//        JsonLoader loader = new JsonLoader(mContext,mHandler,1);
//        loader.load(FileUtils.getDataPathDefaultAssets(mContext ,Comments.BannerImage_DATA_PATH), VitoAdBean.class);
        mJsonLoader = new JsonLoader(mContext, this);
        getDataFromLocal();
    }

    void getDataFromLocal() {
        RequestVo rv = new RequestVo();
        rv.requestUrl = FileUtils.getDataPathDefaultAssets(mContext, Comments.BannerImage_DATA_PATH);
        rv.requestDataMap = new HashMap<String, String>();
        rv.requestDataMap.put("pageSize", "6");
        rv.requestDataMap.put("adModule", "index");
        Log.i("ch", "............" + rv);
        mJsonLoader.load(rv, null, new TypeReference<VitoAdBean>() {
        }, JsonLoader.MethodType.MethodType_Post, GET_FROM_LOCAL);
    }

    void getDataFromServer() {
        RequestVo rv = new RequestVo();
        rv.requestUrl = "http://123.138.87.158:60080/ecs/common/ad/getAd.htm";
        rv.requestDataMap = new HashMap<String, String>();
        rv.requestDataMap.put("pageSize", "6");
        rv.requestDataMap.put("adModule", "index");
        mJsonLoader.load(rv, null, new TypeReference<VitoAdBean>() {
        }, JsonLoader.MethodType.MethodType_Post, GET_FROM_SERVER);
    }

    void initViews(VitoAdBean in_bean) {
        if (in_bean.getCode() != 0)
            return;

        mAdData = (VitoAdBean.VitoAdData) in_bean.getAdDataList();
        final List<BannerItem> imgpath = new ArrayList<BannerItem>();
        if (mAdData == null || mAdData.getRows() == null) {
            return;
        }
        for (VitoAdBean.VitoAdData.VitoADBean temp : mAdData.getRows()) {
            BannerItem item = new BannerItem();
            item.imgUrl = temp.isPiLocal() ? temp.getAdPic() : Comments.BASE_URL + temp.getAdPic();
            item.title = temp.getAdTitle();
            item.id = temp.getId();
            imgpath.add(item);
        }

        if (imgpath.size() <= 0)
            return;

        Log.d("qh", "imgpath : " + imgpath.size());
        if (imgpath.size() == 0)
            return;
        mSimpleImageBanner
                .setIndicatorWidth(6)
                .setIndicatorHeight(6)
                .setIndicatorGap(12)
                .setIndicatorCornerRadius(3.5f)
                .setSelectAnimClass(ZoomInEnter.class)
                .setTransformerClass(DepthTransformer.class)
                .barPadding(0, 10, 0, 10)
                .setSource(imgpath)
                .startScroll();
        mSimpleImageBanner.setOnItemClickL(new BaseBanner.OnItemClickL() {
            @Override
            public void onItemClick(int i) {
                if (mAdData == null || mAdData.getRows() == null || mAdData.getRows().get(i) == null || mAdData.getRows().get(i).getAdType().equals("null"))
                    return;

                clickAdNum(i);
                if (mAdData.getRows().get(i).getAdType().equals("shop")) {
                    changeFg("AdvertiseFragment", i);
                } else if (mAdData.getRows().get(i).getAdType().equals("goods")) {
                    changeFg("AdvertiseDetail", i);
                } else if (mAdData.getRows().get(i).getAdType().equals("ad")) {
                    changeFg("URLFragment", i);
                }
            }
        });
    }

    private void changeFg(String name, int index) {
        Fragment fg = FragmentFactory.getInstance().createFragment("com.vito.fragments", name);
        Bundle arg = new Bundle();
        arg.putSerializable("ad_Data", mAdData.getRows().get(index));
        if (name.equals("URLFragment")) {
            arg.putString("BaseUrl", mAdData.getRows().get(index).getAdContent());
            arg.putString("tText", mContext.getResources().getString(R.string.ad_title));
        }
        fg.setArguments(arg);
        changeMainFragment(fg, true);
    }

    private void clickAdNum(int index) {
        JsonLoader loader = new JsonLoader(mContext, this);
        RequestVo rv = new RequestVo();
        rv.requestUrl = Comments.AD_CLICK_NUM_PATH;
        rv.requestDataMap = new HashMap<String, String>();
        rv.requestDataMap.put("adId", mAdData.getRows().get(index).getId());
        loader.load(rv, null, new TypeReference<VitoJsonTemplateBean>() {
        }, JsonLoader.MethodType.MethodType_Post, ADD_CLICK);
    }

    @Override
    public void onPullDownToRefresh() {
        mPullRefreshParentCallBack.noticeRefreshViewCompleted();
    }

    @Override
    public void onPullUpToRefresh() {
        mPullRefreshParentCallBack.noticeRefreshViewCompleted();
    }

    @Override
    public void onJsonDataGetSuccess(Object re_data, int reqcode) {
        Log.i("ch", re_data + "result");
        if (reqcode == GET_FROM_LOCAL) {
            initViews((VitoAdBean) re_data);
            getDataFromServer();
        } else if (reqcode == GET_FROM_SERVER) {
            initViews((VitoAdBean) re_data);
            //         FileUtils.writeJsonDataToFile(mContext, Comments.BannerImage_DATA_PATH, re_data);
        } else if (reqcode == ADD_CLICK) {
//            ToastShow.toastShow("成功", ToastShow.LENGTH_SHORT);
        }
    }

    @Override
    public void onJsonDataGetFailed(int re_code, String re_info, int reqcode) {
        ToastShow.toastShow("load json error", ToastShow.LENGTH_SHORT);
    }
}

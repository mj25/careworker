package com.vito.careworker.controller;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import com.vito.base.action.Action;
import com.vito.base.jsonbean.VitoJsonTemplateBean;
import com.vito.base.utils.network.httplibslc.JsonLoaderCallBack;
import com.vito.careworker.R;

import de.greenrobot.event.EventBus;

/**
 * Created by lenovo on 2016/6/29.
 */
public abstract class BaseCtrller implements JsonLoaderCallBack {
    protected Context mContext;
    protected ViewGroup mRootView;

    public BaseCtrller(Context mContext, ViewGroup mRootView) {
        this.mContext = mContext;
        this.mRootView = mRootView;
    }

    public void changeMainFragment(Fragment in_fg, boolean add_to_stack) {
        FragmentTransaction transaction = ((FragmentActivity) mContext)
                .getSupportFragmentManager().beginTransaction();
        if (add_to_stack)
            transaction.addToBackStack(null);
        transaction.replace(R.id.rootfragcontent, in_fg);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onJsonDataGetSuccess(Object re_data, int reqcode) {
        int code = ((VitoJsonTemplateBean) re_data).getCode();
        if (code == 11) { // session id  过期
            // 重新登录
            Action mss = new Action();
            mss.setmActionType("ReLogin");
            EventBus.getDefault().post(mss);
        }
    }

    @Override
    public void onJsonDataGetFailed(int re_code, String re_info, int reqcode) {

    }

}

package com.vito.careworker.controller;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import com.vito.careworker.R;
import com.vito.careworker.interfaces.PullRefreshParentCallBack;
import com.vito.careworker.interfaces.PullRefreshSubViewCallBack;

/**
 * Created by lenovo on 2016/6/29.
 */
public abstract class BaseRefreshableCtrller implements PullRefreshSubViewCallBack {
    protected Context mContext;
    protected PullRefreshParentCallBack mPullRefreshParentCallBack;
    protected ViewGroup mRootView;
    public BaseRefreshableCtrller(Context activity, ViewGroup rootView , PullRefreshParentCallBack parentrefresh){
        mContext = activity;
        mPullRefreshParentCallBack = parentrefresh;
        mRootView = rootView;
    }


    public void changeMainFragment(Fragment in_fg, boolean add_to_stack) {
        FragmentTransaction transaction = ((FragmentActivity) mContext)
                .getSupportFragmentManager().beginTransaction();
        if (add_to_stack)
            transaction.addToBackStack(null);
        transaction.replace(R.id.rootfragcontent, in_fg);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public abstract void onPullDownToRefresh() ;
    @Override
    public abstract void onPullUpToRefresh() ;
}

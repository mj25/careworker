package com.vito.careworker.helpers;

import android.content.Context;

import com.vito.base.action.Action;
import com.vito.base.action.FunActionDes;
import com.vito.base.utils.FileUtils;
import com.vito.base.utils.network.httplibslc.JsonLoaderCallBack;
import com.vito.base.utils.network.httplibslc.RequestVo;
import com.vito.base.utils.network.jsonpaser.JsonLoader;
import com.vito.careworker.utils.Comments;

import org.codehaus.jackson.type.TypeReference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2016/7/11.
 */
public class HomeMoreFunHelper implements JsonLoaderCallBack {

    static HomeMoreFunHelper mThis = null;
    List<FunActionDes> mDataList;
    Context mContext;
    JsonLoader mJsonLoader;

    private HomeMoreFunHelper() {
    }

    public static HomeMoreFunHelper getInstance() {
        if (mThis == null)
            mThis = new HomeMoreFunHelper();

        return mThis;
    }

    public void init(Context ctx) {
        mContext = ctx;
        mJsonLoader = new JsonLoader(mContext, this);
        RequestVo rv = new RequestVo();
        rv.requestUrl = FileUtils.getDataPathDefaultAssets(mContext, Comments.HOME_MORE_FUN_CONFIG);
        mJsonLoader.load(rv, null, new TypeReference<ArrayList<FunActionDes>>() {
        }, null, 1);
    }

    @Override
    public void onJsonDataGetSuccess(Object re_data, int reqcode) {
        mDataList = (List<FunActionDes>) re_data;
    }

    @Override
    public void onJsonDataGetFailed(int re_code, String re_info, int reqcode) {

    }

    public Action getActionByTag(String in_tag) {
        if (mDataList != null && mDataList.size() > 0) {
            for (int i = 0; i < mDataList.size(); i++) {
                FunActionDes map = mDataList.get(i);
                if (in_tag.equals(map.action_tag)) {
                    return (Action) map.action;
                }
            }
        }
        return null;
    }
}

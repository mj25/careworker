package com.vito.careworker.helpers;

import android.content.Context;

import com.vito.base.utils.FileUtils;
import com.vito.base.utils.network.httplibslc.JsonLoaderCallBack;
import com.vito.base.utils.network.httplibslc.RequestVo;
import com.vito.base.utils.network.jsonpaser.JsonLoader;
import com.vito.careworker.data.GroupFuns;
import com.vito.careworker.utils.Comments;

import org.codehaus.jackson.type.TypeReference;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by pc on 2016/10/31.
 */
public class MoreHelper implements JsonLoaderCallBack {
    public static final int GET_MORE = 8;
    static private MoreHelper mThis = null;
    public Map<String, GroupFuns> mMapdata;
    boolean isLoading;
    ArrayList<WeakReference<MoreHelperCallBack>> weakReferenceArrayList = new ArrayList<>();
    private JsonLoader mJsonLoader;
    private Context mContext;

    static public MoreHelper getInstance() {
        if (mThis == null)
            mThis = new MoreHelper();
        return mThis;
    }

    public void init(Context ctx) {
        mContext = ctx;
        mJsonLoader = new JsonLoader(mContext, this);
    }

    public void getData(MoreHelperCallBack in_callback) {
        WeakReference<MoreHelperCallBack> jsonLoaderCallBack = new WeakReference<MoreHelperCallBack>(in_callback);
        if (!weakReferenceArrayList.contains(jsonLoaderCallBack))
            weakReferenceArrayList.add(jsonLoaderCallBack);


        if (isLoading) {
            return;
        }

        if (mMapdata == null) {
            RequestVo rv = new RequestVo();
//            rv.requestUrl = Comments.NAVIGATION_URL;
            rv.requestUrl = FileUtils.getDataPathDefaultAssets(mContext, Comments.NAVIGATION_URL);
            mJsonLoader.load(rv, null, new TypeReference<Map<String, GroupFuns>>() {
            }, null, GET_MORE);
            isLoading = true;
        } else if (mMapdata != null) {
            for (int i = 0; i < weakReferenceArrayList.size(); i++) {
                if (weakReferenceArrayList.get(i) != null && weakReferenceArrayList.get(i).get() != null) {
                    weakReferenceArrayList.get(i).get().MoretOk(mMapdata);
                } else {
                    if (weakReferenceArrayList.get(i) != null && weakReferenceArrayList.get(i).get() != null)
                        weakReferenceArrayList.get(i).get().MoreFail();
                }
            }
            weakReferenceArrayList.clear();
        }

    }

    @Override
    public void onJsonDataGetSuccess(Object re_data, int reqcode) {
        isLoading = false;
        mMapdata = (Map<String, GroupFuns>) re_data;
        for (int i = 0; i < weakReferenceArrayList.size(); i++) {
            if (weakReferenceArrayList.get(i) != null && weakReferenceArrayList.get(i).get() != null) {
                weakReferenceArrayList.get(i).get().MoretOk(mMapdata);
            } else {
                if (weakReferenceArrayList.get(i) != null && weakReferenceArrayList.get(i).get() != null)
                    weakReferenceArrayList.get(i).get().MoreFail();
            }
        }
        weakReferenceArrayList.clear();
    }

    @Override
    public void onJsonDataGetFailed(int re_code, String re_info, int reqcode) {
        isLoading = false;
        for (int i = 0; i < weakReferenceArrayList.size(); i++) {
            if (weakReferenceArrayList.get(i) != null && weakReferenceArrayList.get(i).get() != null)
                weakReferenceArrayList.get(i).get().MoreFail();
        }
        weakReferenceArrayList.clear();
    }

    public interface MoreHelperCallBack {
        void MoretOk(Map<String, GroupFuns> re_data);

        void MoreFail();
    }
}

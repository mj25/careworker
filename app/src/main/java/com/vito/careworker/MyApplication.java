package com.vito.careworker;

import android.support.multidex.MultiDexApplication;

import com.vito.base.VitoBaseLib;
import com.vito.careworker.account.LoginCtrller;

/**
 * Created by lenovo24 on 2017/4/19.
 */

public class MyApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        VitoBaseLib.init(this);
        LoginCtrller.getInstance().init(this);
    }
}

package com.vito.careworker.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flyco.banner.anim.select.ZoomInEnter;
import com.flyco.banner.transform.DepthTransformer;
import com.vito.base.ui.fragments.BaseFragment;
import com.vito.base.ui.widget.banner.SimpleGuideBanner;
import com.vito.careworker.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * Created by lenovo24 on 2017/4/20.
 */
@ContentView(R.layout.fg_guide)
public class GuideFragment extends BaseFragment {

    @ViewInject(R.id.sgb)
    SimpleGuideBanner sgb;

    boolean mHasChangedFragment = false;

    //引导图片资源
    public static ArrayList<Integer> getUserGuides() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(R.drawable.yindaoye1);
        list.add(R.drawable.yindaoye2);
        list.add(R.drawable.yindaoye3);
        return list;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void InitContent() {
        sgb.setIndicatorWidth(6)
                .setIndicatorHeight(6)
                .setIndicatorGap(12)
                .setIndicatorCornerRadius(3.5f)
                .setSelectAnimClass(ZoomInEnter.class)
                .setTransformerClass(DepthTransformer.class)
                .barPadding(0, 10, 0, 10)
                .setSource(getUserGuides())
                .startScroll();
        sgb.setOnJumpClickL(new SimpleGuideBanner.OnJumpClickL() {
            @Override
            public void onJumpClick() {
                startMainActivity();
            }

            @Override
            public void onCountDownEnd() {
                startMainActivity();
            }
        });
    }

    private synchronized void startMainActivity() {
        if(mHasChangedFragment == false){
            changeMainFragment(LoginFragment.class, false);
            mHasChangedFragment = true;
        }
    }
}

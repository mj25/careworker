package com.vito.careworker.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.vito.base.ui.fragments.BaseFragment;
import com.vito.base.ui.fragments.FragmentFactory;
import com.vito.base.utils.ToastShow;
import com.vito.careworker.R;
import com.vito.careworker.account.LoginCtrller;
import com.vito.careworker.account.LoginInfo;
import com.vito.careworker.account.LoginResultCallBack;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

/**
 * Created by lenovo24 on 2017/4/19.
 */
@ContentView(R.layout.fg_login)
public class LoginFragment extends BaseFragment {
    @ViewInject(R.id.cb_login)
    CheckBox mAutoCheckBox;
    @ViewInject(R.id.cb_remember_password)
    CheckBox mRememberCheckbox;
    boolean mIsAutoLogin = false;
    boolean mIsRememberPwd = false;
    LoginResultCallBack mloginback = new LoginResultCallBack() {
        @Override
        public void LoginSuccess(String in_str) {
            hideWaitDialog();
            BaseFragment vitoRootFragment = (BaseFragment) FragmentFactory
                    .getInstance().createFragment(MainActivityFragment.class);
            changeMainFragment(vitoRootFragment, false);
        }


        @Override
        public void LoginFail(String re) {
            // TODO Auto-generated method stub
            ToastShow.toastShow(re, ToastShow.LENGTH_SHORT);
            Log.d("jsonStr:", re);
            hideWaitDialog();
        }

        @Override
        public void PushDeviceTokenOk() {

        }

        @Override
        public void PushDeviceTokenFail() {

        }
    };
    @ViewInject(R.id.et_username)
    private EditText mUnameEditText;
    @ViewInject(R.id.et_password)
    private EditText mPWDEditText;
    @ViewInject(R.id.btn_sign_up)
    private Button mSignUpBtn = null;
    @ViewInject(R.id.btn_reset_pwd)
    private Button mResetBtn = null;
    private ILoginSuccess mLoginSuccess = null;
    private LoginInfo mLoginInfo = null;
    private String mUnameStr = null;
    //    ECInitParams.LoginAuthType mLoginAuthType = ECInitParams.LoginAuthType.NORMAL_AUTH;
    private String mPwdStr = null;

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        changeSystemStatusBarBg(R.color.colorPrimary2);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void InitContent() {
        mLoginInfo = LoginInfo.getInstance();
        mPWDEditText.setTransformationMethod(PasswordTransformationMethod
                .getInstance());
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // 记住密码
        mIsRememberPwd = mLoginInfo.isRememberPassword();
        // 自动登录
        mIsAutoLogin = mLoginInfo.isAutoLogin();
        if (mIsRememberPwd) {
            String username = mLoginInfo.getUsername();
            String password = mLoginInfo.getPassword();
            mUnameEditText.setText(username);
            mPWDEditText.setText(password);
            mRememberCheckbox.setChecked(true);
        }

        if (mIsAutoLogin) {
            mAutoCheckBox.setChecked(true);
        }

        // 是否自动登录
        mAutoCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                // TODO Auto-generated method stub
                if (isChecked)
                    mRememberCheckbox.setChecked(true);
            }
        });

        // 是否记住密码
        mRememberCheckbox
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (!isChecked)
                            mAutoCheckBox.setChecked(false);
                    }
                });
        // 登录按钮

        mSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SignUpFragment signUpFragment = (SignUpFragment) FragmentFactory.getInstance().createFragment("com.vito.fragments", "SignUpFragment");
//                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.rootfragcontent, signUpFragment);
//                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//                transaction.commitAllowingStateLoss();

//                changeMainFragment(SignUpFragment.class, false);
            }
        });
        mResetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ResetPwdFragment resetPwdFragment = (ResetPwdFragment) FragmentFactory.getInstance().createFragment("com.vito.fragments", "ResetPwdFragment");
//                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.rootfragcontent, resetPwdFragment);
//                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//                transaction.commitAllowingStateLoss();
            }
        });

        if (mIsAutoLogin)
            login(null);

//        mUnameEditText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                try {
//                    String temp = s.toString();
//                    String tem = temp.substring(temp.length() - 1, temp.length());
//                    char[] temC = tem.toCharArray();
//                    int mid = temC[0];
//                    if (mid >= 48 && mid <= 57) {//数字
//                        return;
//                    }
//                    if (mid >= 65 && mid <= 90) {//大写字母
//                        return;
//                    }
//                    if (mid > 97 && mid <= 122) {//小写字母
//                        return;
//                    }
//                    s.delete(temp.length() - 1, temp.length());
//                } catch (Exception e) {
//
//                }
//            }
//        });
    }

    @Event(value = R.id.tv_jump)
    private void jump(View in_view) {
        BaseFragment vitoRootFragment = (BaseFragment) FragmentFactory
                .getInstance().createFragment(MainActivityFragment.class);
        changeMainFragment(vitoRootFragment, false);
    }

    @Event(value = R.id.btn_login)
    private void login(View in_view) {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            if (getActivity() != null) {
                if (getActivity().getCurrentFocus() != null) {
                    imm.hideSoftInputFromWindow(getActivity()
                                    .getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }

        /* start login */
        mUnameStr = mUnameEditText.getText().toString().trim();
        mPwdStr = mPWDEditText.getText().toString().trim();

//        if (!StringUtil.isMobileNO(mUnameStr)) {
//            ToastShow.toastShow(R.string.mobile_no_error, ToastShow.LENGTH_SHORT);
//            return;
//        }

        if (mRememberCheckbox.isChecked()) {
            mLoginInfo.rememberPassword(true);
            mLoginInfo.saveUserInfo(mUnameStr, mPwdStr);
        } else {
            mLoginInfo.rememberPassword(false);
            mLoginInfo.saveUserInfo("", "");
        }

        mLoginInfo.autoLogin(mAutoCheckBox.isChecked());
        if (TextUtils.isEmpty(mUnameStr) || TextUtils.isEmpty(mPwdStr)) {
            Toast.makeText(getActivity(), R.string.empty_message,
                    Toast.LENGTH_SHORT).show();
        } else if (mPwdStr.length() < 6 && mPwdStr.length() > 16) {
            Toast.makeText(getActivity(), R.string.sign_up_pwd_rule,
                    Toast.LENGTH_SHORT).show();
        } else {
            showWaitDialog();
            LoginCtrller.getInstance().setmCallBack(mloginback);
            LoginCtrller.getInstance().prepareLogin(mUnameStr, mPwdStr);
        }
    }

    public void setILoginSuccess(ILoginSuccess iLoginSuccess) {
        mLoginSuccess = iLoginSuccess;
    }

    @Override
    public void onJsonDataGetSuccess(Object re_data, int reqcode) {
    }

    @Override
    public void onJsonDataGetFailed(int re_code, String re_info, int reqcode) {
    }

    public interface ILoginSuccess {
        public void loginSuccess();
    }
}

package com.vito.careworker.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vito.base.ui.fragments.BaseFragment;
import com.vito.careworker.R;

import org.xutils.view.annotation.ContentView;

/**
 * Created by lenovo24 on 2017/4/20.
 */
@ContentView(R.layout.fg_news)
public class NewsFragment extends BaseFragment {
    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void InitActionBar() {
        super.InitActionBar();
        mLeftView.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void InitContent() {
        super.InitContent();
    }
}

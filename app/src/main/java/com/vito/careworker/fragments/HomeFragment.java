package com.vito.careworker.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.vito.base.ui.fragments.BaseFragment;
import com.vito.base.ui.widget.VitoOnChangeScrollView;
import com.vito.careworker.R;
import com.vito.careworker.controller.ImageBannerCtrller;
import com.vito.careworker.controller.QuickEntiyCtrller;
import com.vito.careworker.controller.ViewPagerController;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

/**
 * Created by lenovo24 on 2017/4/20.
 */
@ContentView(R.layout.fg_home)
public class HomeFragment extends BaseFragment {
    @ViewInject(R.id.refresh_scrollview)
    protected VitoOnChangeScrollView mScrollView;
    @ViewInject(R.id.ll_content)
    protected LinearLayout mContentLayout;
    ImageBannerCtrller mBannerctrller;
    private ViewPagerController mPagerController;
    private QuickEntiyCtrller mQuickEntiyCtrller;

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void InitActionBar() {
        super.InitActionBar();
        mLeftView.setVisibility(View.INVISIBLE);
        mTitle.setText("首页");
    }

    @Override
    protected void InitContent() {
        super.InitContent();
        mBannerctrller = new ImageBannerCtrller(getActivity(), mContentLayout, null);
        mPagerController = new ViewPagerController(getActivity(),mContentLayout,null);
        mQuickEntiyCtrller = new QuickEntiyCtrller(getActivity(), mContentLayout, "work_second");
    }
}

package com.vito.careworker.interfaces;

/**
 * Created by lenovo on 2016/6/29.
 */
public interface PullRefreshParentCallBack {
    void noticeRefreshViewCompleted();
}

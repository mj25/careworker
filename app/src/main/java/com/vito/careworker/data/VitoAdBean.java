package com.vito.careworker.data;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class VitoAdBean implements Serializable {
	@JsonProperty("code")
	private int code;

	@JsonProperty("msg")
	private String msg;

	@JsonProperty("msgId")
	private String msgId;

	@JsonProperty("data")
	private VitoAdData mAdDataList;

	public int getCode() {
		return code;
	}

	public VitoAdData getAdDataList() {
		return mAdDataList;
	}

	public void setAdDataList(VitoAdData adDataList) {
		mAdDataList = adDataList;
	}

	public static class VitoAdData implements Serializable{
		@JsonProperty("total")
		int total;
		@JsonProperty("rows")
		List<VitoADBean> rows;

		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public List<VitoADBean> getRows() {
			return rows;
		}

		public void setRows(List<VitoADBean> rows) {
			this.rows = rows;
		}
		public static class VitoADBean implements Serializable{
			@JsonProperty("adId")
			String id;

			@JsonProperty("adTitle")
			String adTitle;

			@JsonProperty("adPic")
			String adPic;

			@JsonProperty("adType")
			String adType;

			@JsonProperty("shopId")
			String shopId;

			@JsonProperty("goodsId")
			String goodsId;

			@JsonProperty("commodityId")
			String commodityId;

			@JsonProperty("adContent")
			String adContent;

			@JsonProperty("piLocal")
			boolean piLocal;

			public boolean isPiLocal() {
				return piLocal;
			}

			public String getGoodsId() {
				return goodsId;
			}

			public String getId() {
				return id;
			}

			public String getAdTitle() {
				return adTitle;
			}

			public String getAdPic() {
				return adPic;
			}

			public String getAdType() {
				return adType;
			}

			public String getShopId() {
				return shopId;
			}

			public String getCommodityId() {
				return commodityId;
			}

			public String getAdContent() {
				return adContent;
			}
		}
}
}
package com.vito.careworker.data;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created by lenovo on 2016/12/13.
 */
public class GroupFuns {
    @JsonProperty("tab_name")
    private String tab_name;
    @JsonProperty("tabs")
    private List<FunItem> tabs;

    public String getTab_name() {
        return tab_name;
    }

    public List<FunItem> getTabs() {
        return tabs;
    }
}
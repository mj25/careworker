package com.vito.careworker.data;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by pc on 2016/11/28.
 */
public class FunItem {

    @JsonProperty("moduleName")
    private String modulename;
    @JsonProperty("modulePic")
    private String modulepic;
    @JsonProperty("moduleUrl")
    private String moduleurl;
    @JsonProperty("moduleType")
    private String moduletype;

    public String getModulename() {
        return modulename;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    public String getModulepic() {
        return modulepic;
    }

    public void setModulepic(String modulepic) {
        this.modulepic = modulepic;
    }

    public String getModuleurl() {
        return moduleurl;
    }

    public void setModuleurl(String moduleurl) {
        this.moduleurl = moduleurl;
    }

    public String getModuletype() {
        return moduletype;
    }

    public void setModuletype(String moduletype) {
        this.moduletype = moduletype;
    }


}
